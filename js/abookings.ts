/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

declare let Drupal: any;
declare let jQuery: any;
declare let moment: any;

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - https://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {

/**
  CONTENTS

  .set_up_dialogs()
  .add_booking_form_submit_handler()
  .add_field_listeners()
  .check_num_guests()
  .check_date_field_valid_value()
  .add_booking_data_fetched_listener()
  .setup_addons()
  .get_addons_total_guests()
  .calculate_max_guests()
  .set_up_calendar()
  .refresh_main_info()
  .save_booking()
  .show_thank_you_dialog()
  .get_addons_sum()
  .get_addons_total()

  Drupal.abookings
    .get_bookable_nid()
    .get_bookable_data()
    .set_booking_form()
    .find_page_elements()
    .dayHasPrice()
    .dayRender()
    .dayHover()
    .get_seasonal_price()
    .fetch_bookable_data()
    .validate_promo()
    .add_booking()
    .date_unix_to_iso()
    .create_booking()
    .calculate_base_cost()
    .check_event_overlap()
    .invoke_callbacks()
    .validate_booking_form()
 */

let page_setup = false,
  drupalSettings;

let calendar,
  booking_form,
  events = {},
  price_prefix = 'R',
  price_suffix = '',
  discount = 0,
  is_valid_promo = true,
  max_guests = null;

let date_arrive,
  date_depart;

// When calendar is clicked, first click used for arrival, second for departure.
let last_calendar_click = null;

let bookable_field,
  arrival_field,
  departure_field,
  nights_field,
  num_guests_field,
  num_nights_field,
  addons_field,
  base_cost_field,
  promo_field,
  booking_info_container;

let bookable_data = {};
let is_bookable_data_fetched = false;

// To understand behaviors, see https://drupal.org/node/756722#behaviors
Drupal.behaviors.abookings = {
  attach: function(context, settings): void {
    if (page_setup) {
      return;
    }
    // console.log('context: ', context);
    // console.log('settings: ', settings);
    // console.log('drupalSettings: ', drupalSettings);

    drupalSettings = settings;

    switch (drupalSettings.route.name) {
      case 'abookings.book_page':
        Drupal.abookings.set_booking_form('#node-booking-form', context);
        break;
      case 'entity.node.edit_form':
        Drupal.abookings.set_booking_form('#node-booking-edit-form', context);
        break;
      case 'node.add':
        Drupal.abookings.set_booking_form('#node-booking-form', context);
        break;
    }

    Drupal.abookings.find_page_elements(context);

    if (calendar.length >= 1) {
      set_up_calendar(context);
    }

    const backend_url = drupalSettings.booking_settings['backend_url'];
    const bookable_nid = Drupal.abookings.get_bookable_nid();

    if (! is_bookable_data_fetched) {
      const seasonsRequest =   Drupal.abookings.fetch_bookable_data('seasons', bookable_nid, backend_url);
      const bookingsRequest =  Drupal.abookings.fetch_bookable_data('bookings', bookable_nid, backend_url);
      const addonsRequest =    Drupal.abookings.fetch_bookable_data('addons', bookable_nid, backend_url);
      const bookablesRequest = Drupal.abookings.fetch_bookable_data('bookables', 'all', backend_url);
      is_bookable_data_fetched = true;
    }

    set_up_dialogs(context);

    add_booking_data_fetched_listener();

    // Define hover listeners

    const calendar_days = $(calendar).find('.fc-day');
    // console.log('calendar_days: ', calendar_days);

    calendar_days.on('mouseenter', function() {
      // calendar_days.removeClass('active');
      // $(this).addClass('active');
    });
    calendar_days.on('mouseleave', function() {
      // $(this).removeClass('active');
    });

    // Define listeners

    add_booking_form_submit_handler(context);

    add_field_listeners(context);

    refresh_main_info();

    // show_thank_you_dialog('test', booking_info_container);
    page_setup = true;
  }
};


/**
 * Abstracted service for showing messages.
 * 
 * @param  string type
 *   'success', 'info', 'warning', or 'error'
 * @param  string message
 */
function show_message(type, message): void {
  alert(message);
}


function set_up_dialogs(context): void {
  // Bind a click handler on the ctools modal dialog's backdrop that closes that modal.
  $(document).on('dialog_opened', function(event) {
    // Bind a click handler on the modal dialogs' backdrops that closes modals.
    $('.ui-widget-overlay', context).once('backdrop-close-modal').click(function() {
      // console.log('.ui-widget-overlay.click()');

      // to_dialog.dialog( "close" ); // @todo fix this
    });
  });
}


function add_booking_form_submit_handler(context): void {
  booking_form.once('booking_form:on_submit').on('submit', function(event) {
    if (drupalSettings.route.name == 'entity.node.edit_form'
      || drupalSettings.route.name == 'node.add') {
      return;
    }

    if (! is_valid_promo) {
      show_message('error', 'Promo code is not valid.');
      return;
    }

    // Save the booking using AJAX

    event.preventDefault();

    const is_valid = Drupal.abookings.validate_booking_form();
    if (! is_valid) {
      return;
    }

    const serialized_form = $(this).serializeArray();
    // console.log('serialized_form: ', serialized_form);

    const data_object = Drupal.abookings.create_booking(serialized_form);
    // console.log('data_object: ', data_object);
    save_booking(data_object, 'booking');
  });
}


/**
 * Listener functions for the form elements such as the "Arrival date" field.
 */
function add_field_listeners(context): void {
  // Define field value change listeners
  // console.log('add_field_listeners()');

  num_guests_field.change(function() {
    // Check that there's bookable data
    if (jQuery.isEmptyObject(Drupal.abookings.get_bookable_data())) {
      show_message('warning', 'Please select a bookable unit');
      return;
    }

    const change_okay = check_num_guests();
    if (! change_okay) {
      return;
    }
    refresh_main_info();
  });


  arrival_field.change(function() {
    // Calculate number of nights
    date_arrive = moment(arrival_field.val());
    date_depart = moment(departure_field.val());
    const num_nights = date_depart.diff(date_arrive, 'days');
    if (num_nights <= 0) {
      show_message('warning', 'Departure date must be after arrival date');
      num_nights_field.val('');
      return;
    }

    num_nights_field.val(num_nights);
    refresh_main_info();
  });
  departure_field.change(function() {
    // Calculate number of nights
    date_arrive = moment(arrival_field.val());
    date_depart = moment(departure_field.val());
    const num_nights = date_depart.diff(date_arrive, 'days');
    if (num_nights <= 0) {
      show_message('warning', 'Departure date must be after arrival date');
      num_nights_field.val('');
      return;
    }

    num_nights_field.val(num_nights);
    refresh_main_info();
  });


  num_nights_field.change(function() {
    // Calculate + set depart date
    const num_nights = num_nights_field.val();
    date_arrive = moment(arrival_field.val());
    date_depart = moment(date_arrive);
    date_depart.add(num_nights, 'day');

    departure_field.val(date_depart.format('Y-MM-DD'));
    refresh_main_info();
  });

  addons_field.change(function() {
    check_num_guests();
    refresh_main_info();
  });

  bookable_field.change(function() {
    refresh_main_info();
  });

  $('#validate_promo', context).once('validate_promo').click(function(event) {
    event.preventDefault();
    // console.log('promo_code: ', promo_code);
    Drupal.abookings.validate_promo();
  });
};


/**
 * Determines if the number of guests being booked is valid.
 * 
 * @return boolean
 */
function check_num_guests(): boolean {
  calculate_max_guests();
  const num_guests = parseInt(num_guests_field.val());

  // console.log('max_guests: ', max_guests);
  if (num_guests > max_guests) {
    show_message('warning', 'Sorry, you cannot book for more than ' + max_guests + ' guests.');
    num_guests_field.val(max_guests);
    return false;
  }

  const min_guests = parseInt(Drupal.abookings.get_bookable_data()['field_min_guests']);
  // console.log('min_guests: ', min_guests);
  if (num_guests < min_guests) {
    show_message('warning', 'Sorry, you cannot book for fewer than ' + min_guests + ' guests.');
    num_guests_field.val(min_guests);
    return false;
  }
  return true;
}


function check_date_field_valid_value(field): void {
  const date_arrive = moment(arrival_field.val());
  // console.log('date_arrive: ', date_arrive);
  const date_depart = moment(departure_field.val());
  // console.log('date_depart: ', date_depart);

  // Determine number of nights
  let num_nights;

  if (date_arrive._isValid && date_depart._isValid) {
    num_nights = date_depart.diff(date_arrive, 'days');
  }
  else {
    return;
  }
  // console.log('num_nights: ', num_nights);

  let bookable_data = Drupal.abookings.get_bookable_data();
  // console.log('bookable_data: ', bookable_data);
  let min_nights = parseInt(bookable_data['field_min_guests']);
  min_nights = (min_nights) ? min_nights : 1;
  // console.log('min_nights: ', min_nights);
  let min_nights_valid = num_nights >= min_nights;
  if (! min_nights_valid) {
    show_message('warning', 'Sorry, you cannot book for fewer than ' + min_nights + ' nights.');
    field.val(null);
    return;
  }
}


function add_booking_data_fetched_listener(): void {
  $(document).on('bookable_data_fetched', function(element, type) {
    // console.log('bookable_data_fetched, type: ', type);

    if (calendar.length > 0) {
      // Force re-render of calendar days
      calendar.fullCalendar('next');
      calendar.fullCalendar('prev');
    }

    if (type === 'seasons') {}
    else if (type === 'bookables') {
      calculate_max_guests();
    }
    else if (type === 'addons') {
      // console.log("bookable_data['addons']: ", bookable_data['addons']);

      // Hide all the addons
      let addon_checkboxes = $('input[name^="field_addons"]');

      // If there are no checkboxes, make them from fetched data
      if (addon_checkboxes.length == 0) {
        $.each(bookable_data['addons'], function(index, addon_data) {
          // console.log('addon_data: ', addon_data);

          let checkbox = $('<div class="form-item form-type-checkbox" style="display: block;"></div>')
            .append('<input id="edit-field-addons-' + addon_data.nid + '" '
              + 'name="field_addons[' + addon_data.nid + ']" '
              + 'value="' + addon_data.nid + '" '
              + 'class="form-checkbox" type="checkbox">')
            .append('<label for="edit-field-addons-' + addon_data.nid + '" '
              + 'class="option">Cottage</label>');

          $('.field--name-field-addons .form-checkboxes').append(checkbox);
        });

        addons_field = booking_form.find('input[name^="field_addons"]');
        addons_field.change(function() {
          refresh_main_info();
        });
        addon_checkboxes = $('input[name^="field_addons"]');
        // console.log('addon_checkboxes: ', addon_checkboxes);
      }

      addon_checkboxes.parents('.form-type-checkbox').hide();

      const currency_prefix = 'R';
      const currency_suffix = '';

      // For all addons the selected bookable has, show them, append cost
      addon_checkboxes.each(function(index, checkbox) {
        // console.log('checkbox: ', checkbox);
        const nid = $(checkbox).attr('value');
        $.each(bookable_data['addons'], function(index, addon_data) {
          if (addon_data.nid == nid) {
            let price_type = addon_data.field_price_type;
            if (price_type == 'once-off') {price_type = 'once-off';}
            if (price_type == 'flat') {price_type = 'per night';}
            if (price_type == 'guest-based') {price_type = 'per night per guest';}

            $(checkbox)
              .parents('.form-type-checkbox').show()
              .find('label')
              .prepend(addon_data.field_image)
              .append(' (' + currency_prefix + addon_data.field_amount + ' ' + price_type + currency_suffix + ')')
              .append('<br><small>' + addon_data.field_short_description + '</small>');

            return false;
          }
        });
      });
      if (bookable_data['addons'].length == 0) {
        $('#edit-field-addons--wrapper .fieldset-wrapper')
          .append('<em>No addons available</em>');
      }

      calculate_max_guests();
    }

    // Display other people's bookings
    else if (type === 'bookings') {
      // console.log("bookable_data['bookings']: ", bookable_data[' bookings']);
      const date = new Date();
      const timestamp_first_of_month = date.setDate(1)/1000; // *60*60*24
      // console.log('timestamp_first_of_month: ', timestamp_first_of_month);

      // If bookable_data hasn't been fetched yet, can't render yet.
      if (typeof bookable_data['bookings'] === 'undefined') {
        return;
      }

      $.each(bookable_data['bookings'], function(index, booking_data) {
        // console.log('booking_data: ', booking_data);
        // calendar.fullCalendar('removeEvents');
        const timestamp_checkout_date = parseInt(booking_data.field_checkout_date);
        // If the checkout date is in the past
        if (timestamp_checkout_date >= timestamp_first_of_month) {
          Drupal.abookings.add_booking(booking_data.nid, booking_data);
        }
      });
    }

  });
}


// function setup_addons() {}


function get_addons_total_guests(): number {
  let guests = 0;
  const addon_checkboxes = $('input[name^="field_addons"]:checked');

  // For all addons the selected bookable has, show them, append cost
  addon_checkboxes.each(function(index, checkbox) {
    // console.log('checkbox: ', checkbox);
    let nid = $(checkbox).attr('value');
    $.each(bookable_data['addons'], function(index, addon_data) {
      // console.log('addon_data: ', addon_data);
      if (addon_data.nid == nid) {
        guests += parseInt(addon_data.field_num_guests);
      }
    });
  });
  // console.log('addons_guests: ', addons_guests);
  return guests;
}


function calculate_max_guests(): void {
  const bookable_data = Drupal.abookings.get_bookable_data();
  // console.log('bookable_data: ', bookable_data);
  if (! bookable_data) {
    // throw 'No booking data in calculate_max_guests()';
    return;
  }
  const bookable_max_guests = parseInt(bookable_data['field_max_guests']);
  // console.log('bookable_max_guests: ', bookable_max_guests);
  const addons_guests = get_addons_total_guests();
  // console.log('addons_guests: ', addons_guests);
  max_guests = bookable_max_guests + addons_guests;
  // console.log('max_guests: ', max_guests);

  const label = num_guests_field.siblings('label');
  if (! label.find('span').length) {
    label.append(" <span></span>");
  }
  label.find('span').html("(max " + max_guests + " guests)");
}



function set_up_calendar(context): void {
  // console.log('set_up_calendar()');

  calendar.fullCalendar({
    dayRender: function(date, cell) {
      Drupal.abookings.dayRender(date, cell);
    },
    eventMouseover: function(event, jsEvent, view) {
      // console.log('hovered');
      // This doesn't work, because days aren't events!
      // Drupal.abookings.dayHover(event, jsEvent, view);
    },
    dayClick: function(date_clicked, jsEvent, view, resourceObj) {
      // console.log('date_clicked: ', date_clicked);

      // let date_arrive,
      //   date_depart;

      // // If the calendar hasn't been clicked, or the last click was for check out
      // // Use clicked date for arrival
      // if (last_calendar_click !== 'arrive') {
      // }
      // // Use clicked day for departure
      // else {
      //   date_depart = date_clicked;
      //   date_arrive = moment(arrival_field.val());
      //   num_nights = date_depart.diff(date_arrive, 'days');
      // }

      // date_arrive = date_clicked;
      // let nights = parseInt(nights_field.val());
      //
      // date_depart = moment(date_arrive);
      // date_depart.add(nights, 'day');
      //
      // dayClickHandler(date_arrive, date_depart);
    }
  });

  $(document, context).on('mousedown', '.fc-day, .fc-day-top', function(jsEvent) {
    const date_clicked = moment($(this).attr('data-date'));
    // console.log('date_clicked: ', date_clicked);

    date_arrive = date_clicked;
    const nights = parseInt(nights_field.val());

    arrival_field.val(date_arrive.format('Y-MM-DD'));
  });


  $(document, context).on('mouseup', '.fc-day, .fc-day-top', function(jsEvent) {
    const date_clicked = moment($(this).attr('data-date'));
    // console.log('date_clicked: ', date_clicked);

    date_depart = date_clicked;
    date_arrive = moment(arrival_field.val());
    if (date_arrive.unix() == date_depart.unix()) {
      date_depart.add(1, 'day');
    }
    const num_nights = date_depart.diff(date_arrive, 'days');
    num_nights_field.val(num_nights);

    departure_field.val(date_depart.format('Y-MM-DD'));

    dayClickHandler(date_arrive, date_depart);
  });
};


/**
 * @param {Object} date_arrive - Moment object
 * @param {Object} date_depart - Moment object
 */
function dayClickHandler(date_arrive, date_depart): void {
  // console.log('dayClickHandler()');
  // console.log('date_arrive: ', date_arrive);
  // console.log('date_depart: ', date_depart);

  // Check that there's bookable data
  if (jQuery.isEmptyObject(Drupal.abookings.get_bookable_data())) {
    show_message('warning', 'Please select a bookable unit');
    return;
  }
  if(! Drupal.abookings.dayHasPrice(date_arrive)) {
    show_message('warning', 'Please select a date that has a price');
    return;
  }

  // If this new event overlaps with any others
  const overlap = Drupal.abookings.check_event_overlap(date_arrive, date_depart);
  if (overlap) {
    return;
  }

  // Create or update the current booking
  const booking_data = {
    field_booking_status: 'provisional',
    field_checkin_date: date_arrive.unix(),
    field_checkout_date: date_depart.unix()
  };
  Drupal.abookings.add_booking('current', booking_data);

  refresh_main_info();

  // if (last_calendar_click == null) {
  //   last_calendar_click = 'arrive';
  // }
  // else if (last_calendar_click == 'arrive') {
  //   last_calendar_click = 'depart';
  // }
  // else {
  //   last_calendar_click = 'arrive';
  // }
}



/**
 * Used by both front-end (Book page) and backend (booking node edit forms).
 */
function refresh_main_info(): void {
  // console.log('refresh_main_info()');
  // console.log('date_arrive: ', date_arrive);

  // const is_from_click = (typeof date_arrive === 'undefined') ? false : true;

  const date_arrive = moment(arrival_field.val());
  // console.log('date_arrive: ', date_arrive);
  const date_depart = moment(departure_field.val());
  // console.log('date_depart: ', date_depart);

  if (date_arrive._isValid) {
    calendar.fullCalendar('gotoDate', date_arrive);
  }

  // Determine number of nights
  let num_nights;

  if (date_arrive._isValid && date_depart._isValid) {
    num_nights = date_depart.diff(date_arrive, 'days');
  }
  else {
    return;
  }
  // console.log('num_nights: ', num_nights);

  const overlap = Drupal.abookings.check_event_overlap(date_arrive, date_depart);
  if (overlap) {
    return;
  }

  // If the is the Book page
  if (drupalSettings.route.name == 'abookings.book_page') {
    // Create or update the current booking (on JS calendar)
    const booking_data = {
      field_booking_status: 'provisional',
      field_checkin_date: date_arrive.unix(),
      field_checkout_date: date_depart.unix()
    };
    // console.log('booking_data: ', booking_data);
    Drupal.abookings.add_booking('current', booking_data);
  }

  // If bookable_data hasn't been fetched yet, can't render yet.
  if (jQuery.isEmptyObject(Drupal.abookings.get_bookable_data())) {
    return;
  }

  // console.log('here1');


  // Base cost
  const base_cost = Drupal.abookings.calculate_base_cost();
  // console.log('base_cost: ', base_cost);
  base_cost_field.val(base_cost); // The hidden field
  const currency_prefix = 'R';

  // .toLocaleString("en-GB", {style: "currency", currency: "GBP", minimumFractionDigits: 2})
  booking_info_container.find('.field_base_cost .value')
    .html(currency_prefix + base_cost);

  // Additions  (not shown)
  // Deductions (not shown)

  // Addons
  const addons_total = get_addons_total(num_nights);
  booking_info_container.find('.field_addons .value')
    .html(currency_prefix + addons_total);

  // Promotions (technically calculated as % of total cost)
  const discount_amount = discount / 100 * base_cost;
  booking_info_container.find('.field_promo_discount .value')
    .html(currency_prefix + discount_amount);

  // Total cost
  booking_info_container.find('.field_total_cost .value')
    .html(currency_prefix + (base_cost - discount_amount + addons_total));



  // console.log('here2');

  // Arrival date
  booking_info_container.find('.field_checkin_date .value')
    .html(date_arrive.format('dddd, D MMMM Y'));

  // Departure date
  booking_info_container.find('.field_checkout_date .value')
    .html(date_depart.format('dddd, D MMMM Y'));

  // Number of nights
  booking_info_container.find('.field_num_nights .value')
    .html(num_nights);
}


/**
 * @returns true if successful, false otherwise.
 */
function save_booking(data_object, type, success_callbacks = null, always_callbacks = null): Promise<any>|null {
  // console.log('data_object: ', data_object);

  let url;
  const api_url = drupalSettings.booking_settings['backend_url'] + '/bookings-api/';
  // console.log('api_url: ', api_url);

  switch (type) {
    case 'booking':
      url = api_url + 'booking';
      // + 'filter[collection]=' + collection_nid
      // + 'range=1000';
      break;

    default:
      throw 'Param "type" not valid in ' + 'Drupal.es_api_interactions.fetch_selectables_data().';
      break;
  }
  // console.log('data: ', data);

  const settings = {
    type: "POST",
    contentType: "application/json",
    data: JSON.stringify(data_object, null, 2),
    url: url
  }
  const jqxhr = $.ajax(settings);
  // show_message('info', 'Placing booking...');
  booking_info_container.find('.loader').removeClass('hidden');

  // Failure
  jqxhr.fail(function( data ) {
    if(jqxhr.readyState < 4)  {
      console.log( "Request was not completed." );
    }
    else {
      console.log( "In jqxhr.fail(), data: ", data );
    }

    const message = 'Sorry, there was a problem with your booking.\n\n'
        + 'Reason: ' + data.responseJSON.error_message;
    show_message('error', message);
  });

  // Success
  jqxhr.done(function( data ) {
    // console.log( 'jqxhr.done(). data: ', data);

    if (typeof data == 'string') {
      const message = 'Sorry, there was a problem with your booking.\n\n'
          + 'Reason: ' + data;
      show_message('error', message);
      return null;
    }

    const node_url = drupalSettings.path.baseUrl + 'node/' + data.nid;
    // console.log('node_url: ', node_url);
    const node_link = '<a href="' + node_url + '">View booking</a>';
    // window.location.replace(node_url);

    booking_info_container.find('.message_container')
      .find('[class*="messages"]').remove();

    show_thank_you_dialog(data.nid, booking_info_container);

    arrival_field.val(null);
    departure_field.val(null);
    num_nights_field.val(null);

    if (success_callbacks) {
      Drupal.abookings.invoke_callbacks(success_callbacks);
    }
  });

  // Always
  jqxhr.always(function( data ) {

    booking_info_container.find('.loader').addClass('hidden');
  });

  return jqxhr;
}


function show_thank_you_dialog(nid, container): void {
  const message_text = '<div><p>Thank you for booking with us! '
    + 'We have sent you an email with your provisional booking information.</p>'
    + '<p>Your reference number is <strong>' + nid + '</strong></p></div>';
  const message = $(message_text)
    .appendTo(container.find('.message_container'));
  // console.log('container: ', container);
  // console.log('message: ', message);

  message.dialog({
      title: 'Booking successful',
      dialogClass: 'no-close',
      modal: true,
      show: 300,
      width: 450,
      buttons: [
        {
          text: "Okay",
          click: function() {
            $( this ).dialog( "close" );
          }
        }
      ]
    });
}



function get_addons_sum(type): number {
  let sum = 0;

  // Loop through addons checkboxes
  $('input[name^="field_addons"]').each(function(element) {

    if (! $(this).is(':checked')) {
      return null;
    }

    const nid = $(this).val();
    // console.log('nid: ', nid);

    // Loop through addons fetched
    $.each(bookable_data['addons'], function(index, addon) {
      if (addon['field_price_type'] == type) {
        sum += parseFloat(addon['field_amount']);
      }
    });
  });

  return sum;
}

function get_addons_total(num_nights): number {
  // console.log('num_nights: ', num_nights);
  let total = 0;

  const num_guests = parseInt(num_guests_field.val());

  const addons_once   = get_addons_sum('once-off'),
      addons_flat   = get_addons_sum('flat'),
      addons_pguest = get_addons_sum('guest-based');

  total += addons_once;

  for (let i = num_nights - 1; i >= 0; i--) {
    total += addons_flat;
    total += addons_pguest * num_guests;
    // console.log('total: ', total);
  }

  return total;
}





Drupal.abookings = {

  get_bookable_nid: function(): void {
    return bookable_field.val();
  },

  /**
   * Gets the data of the active bookable unit.
   */
  get_bookable_data: function(): any|null {
    const bookable_nid = Drupal.abookings.get_bookable_nid();
    // console.log('bookable_nid: ', bookable_nid);
    let active_bookable = null;

    $.each(bookable_data['bookables'], function(index, bookable) {
      // console.log("bookable: ", bookable);
      if (bookable['nid'] == bookable_nid) {
        active_bookable = bookable;
      }
    });
    // if (active_bookable === null) {
    //   throw "There is no active bookable.";
    //   // show_message('warning', 'Please choose a bookable unit before placing a booking.');
    // }
    return active_bookable;
  },

  set_booking_form: function(selector, context): void {
    // console.log('selector: ', selector);

    // console.log('calendar: ', calendar);
    if (typeof booking_form === 'undefined') {
      // booking_form = $('#book-page-form', context);
      booking_form = $(selector, context);
    }
    // console.log('booking_form: ', booking_form);
  },



  find_page_elements: function(context): void {
    // console.log('booking_form: ', booking_form);

    // Define calendar and booking_form
    if (typeof calendar === 'undefined') {
      calendar = $('#calendar', context);
    }

    if (typeof booking_info_container === 'undefined') {
      booking_info_container = $('#booking_info', context);
      // console.log('booking_info_container: ', booking_info_container);

      if (booking_info_container.length == 0) {
        booking_info_container =
          $('<div id="booking_info"><div class="message_container"></div></div>', context)
          .prependTo('body');
      }
      // console.log('booking_info_container: ', booking_info_container);
    }
    // console.log('booking_info_container: ', booking_info_container);

    bookable_field    = booking_form.find('select[name="field_bookable_unit"]');
    arrival_field     = booking_form.find('input[name^="field_checkin_date"]');
    departure_field   = booking_form.find('input[name^="field_checkout_date"]');
    nights_field      = booking_form.find('input[name^="field_num_nights"]');
    num_guests_field  = booking_form.find('input[name^="field_num_guests"]');
    num_nights_field  = booking_form.find('input[name^="field_num_nights"]');
    addons_field      = booking_form.find('input[name^="field_addons"]');
    base_cost_field   = booking_form.find('input[name^="field_base_cost"]');
    promo_field       = booking_form.find('input[name^="field_promo_code_provided"]');
  },


  dayHasPrice: function(date): boolean {
    const date_price = Drupal.abookings.get_seasonal_price(date.unix());
    return date_price ? true : false;
  },


  dayRender: function(date, cell): void {
    // console.log('date: ', date);
    // console.log('cell: ', cell);

    // If bookable_data hasn't been fetched yet, can't render yet.
    if (typeof bookable_data['seasons'] === 'undefined') {
      return;
    }

    let cell_markup = '';
    const num_guests = num_guests_field.val();
    // console.log('num_guests: ', num_guests);

    const date_price = Drupal.abookings.get_seasonal_price(date.unix()) * 1; // Display as integer

    if (date_price) {
      // date_price = date_price * parseInt(num_guests);
      cell_markup = '<p>' + price_prefix + date_price + '</p>';
    }
    else {
      cell_markup = '<p><span title="Price on request">PoR</span></p>';
    }

    $(cell).append(cell_markup);
  },


  dayHover: function(event, jsEvent, view): void {
    // console.log('event: ', event;
    // console.log('jsEvent: ', jsEvent);
    // console.log('view: ', view);
  },


  get_seasonal_price: function(unix_date): number|null {
    let seasonal_price = null;

    // If bookable_data hasn't been fetched yet, can't render yet.
    if (typeof bookable_data['seasons'] === 'undefined') {
      console.log( "Error: bookable_data hasn't been fetched yet" );
      return null;
    }

    // Loop through all seasons

    // console.log(bookable_data, 'bookable_data causing problems?');
    $.each(bookable_data['seasons'], function(index, season_data) {
      // console.log(index, ': ', season_data);

      // If date falls in a season
        if ((unix_date > season_data.field_start_date)
          && (unix_date < season_data.field_end_date)) {
          // Calculate product of season price and guests
          seasonal_price = season_data.field_seasonal_price;
        }
    });

    return seasonal_price;
  },


  /**
   * Fetches data from the backend. Has to be fetched using an HTTP request
   * rather than from drupalSettings because the backend might be a different site.
   *
   * @param type
   *   The type of data to fetch. Can be "seasons", "bookings", "bookables".
   * @param nid
   *   The ID of the bookable unit node.
   * @param drupalSettings
   */
  fetch_bookable_data: function(type, nid, backend_url): Promise<any> {
    // If there is no chosen bookable unit, set nid to 'all'.
    nid = (nid == '_none') ? 'all' : nid;
    const url = backend_url + '/data/' + type + '/' + nid;

    const settings = {
      type: "GET",
      contentType: "application/json",
      url: url
    };
    const jqxhr = $.ajax(settings);

    jqxhr.fail(function( data ) {
      if(jqxhr.readyState < 4)  {
        console.log( "Request was not completed." );
      }
      else {
        console.log( "In jqxhr.fail(), data: ", data );
        show_message('warning', `Sorry, there was a problem loading ${type} data.`);
      }
    });

    jqxhr.done(function( data ) {
      // console.log('Request success. type, data: ', type, data);
      bookable_data[type] = data;
      // console.log('bookable_data: ', bookable_data);
      $(document).trigger('bookable_data_fetched', type);
    });

    jqxhr.always(function( data ) {
      // notification.remove();
    });

    return jqxhr;
  },


  /**
   *
   */
  validate_promo: function(): void {
    // If there is no chosen bookable unit, set nid to 'all'.
    const discount_span = promo_field.parents('[class*="field--type"]')
      .find('.discount');

    const promo_code = promo_field.val();
    const checkin_date = arrival_field.val();
    const nights = num_nights_field.val();

    if (promo_code == '') {
      promo_field.attr('is_valid', '');
      is_valid_promo = true;
      discount_span.html('');
      return;
    }

    promo_field.attr('is_valid', 'loading');

    const data_object = {
      field_promo_code_provided:  promo_code,
      field_checkin_date:         checkin_date,
      field_num_nights:           nights
    }

    const settings = {
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify(data_object, null, 2),
      url: drupalSettings.booking_settings['backend_url'] + '/admin/promos/validate/'
    };
    const jqxhr = $.ajax(settings);

    jqxhr.fail(function( data ) {
      if(jqxhr.readyState < 4)  {
        console.log( "Request was not completed." );
      }
      else {
        console.log( "In jqxhr.fail(), data: ", data );
      }
    });

    jqxhr.done(function( data ) {
      // console.log('Request success. data: ', data);

      if (data.is_valid) {
        // show_message('success', 'Promo code is valid.');
        promo_field.attr('is_valid', 'true');
        is_valid_promo = true;
        discount = data.discount;
        discount_span.html('Discount: ' + discount + '%');
      }
      else {
        // show_message('warning', 'Promo code is NOT valid.');
        promo_field.attr('is_valid', 'false');
        is_valid_promo = false;
        discount_span.html('Discount: 0% (' + data.error_message + ')');
      }
    });

    jqxhr.always(function( data ) {
      // notification.remove();
      refresh_main_info();
    });

  },


  /**
   * Adds a booking to the full-calendar.
   */
  add_booking: function(nid, booking_data): void {
    // console.log('nid: ', nid);
    // console.log(nid, ' -> ', booking_data);
    // console.log('events: ', events);

    if (typeof booking_data === 'undefined'
      || calendar.length === 0
      || booking_data.field_checkout_date < booking_data.field_checkin_date) {
      console.error(`Booking "${nid}" has an invalid check-out date.`);
      return;
    }

    // console.log('adding');
    // If this is a new event (not yet on the calendar)
    if (typeof events[nid] === 'undefined') {
      // console.log('New event: events[nid]: ', events[nid]);
      events[nid] = {
        id: nid,
        field_booking_status: booking_data.field_booking_status,
        start: moment(booking_data.field_checkin_date, 'X'),
        // End 1 day before checkout date
        end: moment(booking_data.field_checkout_date, 'X'), // - 1*60*60*24
        rendered: false
      }
      // events[nid].end.subtract(1, 'days');
      if (nid === 'current') {
        events[nid].title = 'Booking';
        events[nid].color = '#69C0FF'; // light blue
        events[nid].field_booking_status = 'provisional';
      }
      else {
        events[nid].title = 'Unavailable';
        events[nid].color = '#A8A8A8';
      }
      // console.log('events[nid]: ', events[nid]);
    }

    // If this is not new (is on the calendar), and is the current event
    else if (nid === 'current') {
      // console.log('Is current event: events[nid]: ', events[nid]);
      // Update the dates
      events[nid].event[0].start = moment(booking_data.field_checkin_date, 'X');
      events[nid].event[0].end = moment(booking_data.field_checkout_date, 'X');
      // console.log('events[nid].event[0]: ', events[nid].event[0]);
      // Update the event on the calendar
      calendar.fullCalendar('updateEvent', events[nid].event[0]);
      return;
    }
    // console.log('events: ', events);

    // Render event on calendar
    if (! events[nid].rendered) {
      // console.log('rendering events[' + nid + ']');
      events[nid]['event'] = calendar.fullCalendar( 'renderEvent', events[nid], true );
      events[nid].rendered = true;
      // console.log('events[nid] after creation: ', events[nid]);
    }
    // console.log('events', events);
  },


  date_unix_to_iso: function(timestamp): string {
    // If timestamp is a string, parse it as an integer
    timestamp = (typeof timestamp === 'string') ? parseInt(timestamp) : timestamp;
    const date = new Date(timestamp * 1000);
    return date.toISOString();
  },


  /**
   * Creates a booking object that can be saved to fullCalendar.
   */
  create_booking: function(serialized_form): any {
    const data_object_raw = {};
    $.each(serialized_form, function () {
      if (data_object_raw[this.name] !== undefined) {
        if (!data_object_raw[this.name].push) {
          data_object_raw[this.name] = [data_object_raw[this.name]];
        }
        data_object_raw[this.name].push(this.value || '');
      } else {
        data_object_raw[this.name] = this.value || '';
      }
    });
    // console.log('data_object_raw: ', data_object_raw);

    const booking_title =
      data_object_raw['field_first_name[0][value]'] + ' ' +
      data_object_raw['field_last_name[0][value]'] +
      ' (' + data_object_raw['field_num_nights[0][value]'] + ' nights)';

    const data_object = {
      'title': booking_title,
      'field_booking_status':       'provisional', // data_object_raw['field_booking_status[0][value]'],
      'field_paid_status':          'none',
      'field_bookable_unit':        data_object_raw['field_bookable_unit'],
      'field_checkin_date':         data_object_raw['field_checkin_date[0][value][date]'],
      'field_checkout_date':        data_object_raw['field_checkout_date[0][value][date]'],
      'field_first_name':           data_object_raw['field_first_name[0][value]'],
      'field_last_name':            data_object_raw['field_last_name[0][value]'],
      'field_country':              data_object_raw['field_country'],
      'field_email_address':        data_object_raw['field_email_address[0][value]'],
      'field_notes':                data_object_raw['field_notes[0][value]'],
      'field_num_guests':           data_object_raw['field_num_guests[0][value]'],
      'field_num_nights':           data_object_raw['field_num_nights[0][value]'],
      'field_phone_number':         data_object_raw['field_phone_number[0][value]'],
      'field_phone_number_alt':     data_object_raw['field_phone_number_alt[0][value]'],
      'field_promo_code_provided':  data_object_raw['field_promo_code_provided[0][value]'],
      'field_base_cost':            data_object_raw['field_base_cost[0][value]'],
      'field_addons':               []
    };
    // console.log('data_object: ', data_object);

    const addon_checkboxes = $('input[name^="field_addons"]:checked');
    // console.log('addon_checkboxes: ', addon_checkboxes);

    addon_checkboxes.each(function(index, checkbox) {
      // console.log('checkbox: ', checkbox);
      const nid = $(checkbox).attr('value');
      data_object.field_addons.push(nid);
    });

    return data_object;
  },


  /**
   * @param start_date
   *
   * @param end_date
   *   Moment.js date object.
   */
  calculate_base_cost: function(): number|null {
    // console.log('calculate_base_cost()');

    const active_bookable_data = Drupal.abookings.get_bookable_data();
    // console.log('active_bookable_data: ', active_bookable_data);
    if (! active_bookable_data) {
      return null;
    }

    // If there is no current event, quit.
    if ((drupalSettings.route.name == 'abookings.book_page') && (! events.hasOwnProperty('current'))) {
      return null;
    }
    // console.log("events['current']: ", events['current']);

    // start_date and end_date must be Moment.js date objects.

    const start_date = moment(arrival_field.val() + ' 09');
    // console.log('start_date: ', start_date);

    const end_date = moment(departure_field.val() + ' 09');
    // console.log('end_date: ', end_date);

    const t = new Date(); // Use current day and time
    t.setHours(0,0,0,0); // Set time to 00:00am
    const today = moment(t);
    // console.log('today: ', today);

    let total_cost = 0;
    const num_guests = parseInt(num_guests_field.val());
    let guest_multiplier = num_guests;

    // If bookable uses a flat rate (not dependant on number of guests)
    if (active_bookable_data['field_price_type'] == 'flat') {
      guest_multiplier = 1;
    }

    if (! (start_date.unix() > today.unix())) {
      show_message('warning', 'You cannot book a date in the past.');
      return null;
    }
    if (! (end_date.unix() - start_date.unix() > 0)) {
      show_message('warning', 'The dates you selected for your booking are invalid.');
      return null;
    }


    // Loop through each night of the booking (called an iteration)
    const iteration_date = moment(start_date);
    while (iteration_date.unix() < end_date.unix()) {
      const date_price = Drupal.abookings.get_seasonal_price(iteration_date.unix());
      // console.log('date_price: ', date_price);

      // If bookable has a minimum cost, use it...
      // @todo this ^.

      total_cost += date_price * guest_multiplier;

      iteration_date.add(1, 'day');
    }

    if (total_cost == 0) {
      show_message('warning', 'The booking does not fall within a season.');
    }

    return total_cost;
  },


  /**
   * Determines if the event with specified dates overlaps any other event.
   */
  check_event_overlap: function(date_arrive, date_depart): boolean {
    // console.log('events: ', events);

    // Only works using calendar
    if (calendar.length == 0) {
      // console.error('Calendar not found.');
      return false;
    }

    let overlap = false;
    // console.log('events: ', events);
    $.each(events, function(nid, event_data) {
      // console.log('event_data: ', event_data);
      if (nid === 'current') {
        return false;
      }

      // If event end date is missing, it's a 1-day event.
      if (! event_data.event[0].end) {
        event_data.event[0].end = event_data.event[0].start;
      }
      // console.log('event_data: ', event_data);

      const StartA = date_arrive.unix(),
        EndB = event_data.event[0].end.unix(),
        EndA = date_depart.unix(),
        StartB = event_data.event[0].start.unix();

      // console.log(
      //   date_arrive.format('YYYY-MM-DD'),
      //   event_data.event[0].end.format('YYYY-MM-DD'),
      //   date_depart.format('YYYY-MM-DD'),
      //   event_data.event[0].start.format('YYYY-MM-DD'));

      if ((StartA <= EndB) && (EndA >= StartB)) {
        overlap = true;
      }
    });

    if (overlap) {
      show_message('warning', 'Your booking cannot overlap with another booking.');
    }
    return overlap;
  },


  /**
   * Invokes functions provided in an array. Often used by delayed fuctions
   * like .done() and .always() which rely on HTTP requests.
   *
   * @param callbacks
   *   An array of arrays, in the form [[function, thisobject, [args...]], ...]
   */
  invoke_callbacks: function(callbacks): any[] {
    // If callback functions were provided, invoke them.
    if (typeof callbacks !== 'undefined') {
      // console.log('callbacks: ', success_callbacks, always_callbacks);
      $.each(callbacks, function(key, callback_info) {
        // console.log('callback_info: ', callback_info);
        // console.log('this[1]: ', this[1]);

        // If callback's "this" object wasn't provided, provide a default
        this[1] = typeof this[1] === 'undefined' ? this : this[1];
        // If callback's parameters weren't provided, provide a default of an empty array
        this[2] = typeof this[2] === 'undefined' ? [] : this[2];

        // let this_obj = (typeof callback_info[2] === 'undefined') ? this : callback_info[2];
        this[3] = this[0].apply(this[1], this[2]);
      });
    }
    return callbacks;
  },



  validate_booking_form: function(): boolean {
    let is_valid = true;

    if (departure_field.val() <= arrival_field.val()) {
      show_message('warning', 'Departure date must be after arrival date.');
      is_valid = false;
    }
    const base_cost = Drupal.abookings.calculate_base_cost();
    // console.log('base_cost: ', base_cost);
    if (parseFloat(base_cost_field.val()) != base_cost) {
      show_message('warning', 'Base cost is not valid. Please select an arrival and departure date.');
      is_valid = false;
    }
    return is_valid;
  }


}



})(jQuery, Drupal, this, this.document);
